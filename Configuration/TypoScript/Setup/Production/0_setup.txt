#####################
## SETUP HAMMER.JS ##
#####################

[globalVar = LIT:1 = {$plugin.tx_teufels_thm_jq_hammer.settings.lib.hammer.bUseHammer}]
    page {
        includeJSFooterlibs  {

            teufels_thm_jq_hammer__js = {$plugin.tx_teufels_thm_jq_hammer.settings.production.includePath.public}Assets/Js/hammer.min.js
            teufels_thm_jq_hammer__js.async = 1

            teufels_thm_jq_hammer__use_js = {$plugin.tx_teufels_thm_jq_hammer.settings.production.includePath.public}Assets/Js/use_hammer.min.js
            teufels_thm_jq_hammer__use_js.async = 1
        }
    }
[global]