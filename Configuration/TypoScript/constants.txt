########################################
## INCLUDES FOR STAGING && PRODUCTION ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:teufels_thm_jq_hammer/Configuration/TypoScript/Constants/Production" extensions="txt">

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[globalString = ENV:HTTP_HOST=development.*]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:teufels_thm_jq_hammer/Configuration/TypoScript/Constants/Development" extensions="txt">
[global]