/**
 * Created by hd on 07.01.16.
 */
var teufels_thm_jq_hammer__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        if (typeof Hammer != 'undefined' && typeof Hammer == 'function') {

            clearInterval(teufels_thm_jq_hammer__interval);

            if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
                console.info('jQuery - Hammer loaded');
            }

        }

    }

}, 1000);